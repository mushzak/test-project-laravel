<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\StoreInvoiceRequest;
use App\Http\Requests\UpdateInvoiceRequest;
use App\Invoice;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * @param StoreInvoiceRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeInvoice(StoreInvoiceRequest $request)
    {
        $data = $request->all();

        Invoice::create($data);
        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyInvoice($id)
    {
        Invoice::destroy($id);

        return redirect()->back();
    }

    /**
     * @param Invoice $invoice
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editInvoice(Invoice $invoice)
    {
        $user = Auth::user();
        $clients = $user::whereHas('roles', function ($query) {
            $query->where('name', 'client');
        })->get();

        return view('editClientTotal', compact('invoice', 'clients'));
    }

    /**
     * @param UpdateInvoiceRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateInvoice(UpdateInvoiceRequest $request)
    {
        $invoice = Invoice::find($request->id);
        $invoice->total = $request->total;
        $invoice->save();

        return redirect()->route('home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function clientView()
    {
        $user = Auth::user();
        $clients = $user::whereHas('roles', function ($query) {
            $query->where('name', 'client');
        })->get();

        return view('clientView', compact('clients'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyClient($id)
    {
        User::destroy($id);

        return redirect()->back();
    }

    /**
     * @param StoreClientRequest $request
     * @return mixed
     */
    public function createClient(StoreClientRequest $request)
    {
        $data = $request->all();
        $client = Role::where('name', 'client')->select('id')->first();
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $user->roles()->attach($client);

        return redirect()->back();
    }

}
