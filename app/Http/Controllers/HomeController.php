<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $role =  $user->roles[0]->name;
        if ($role == 'client'){
            $invoices = Invoice::with('users')->where('user_id',$user->id)->get();
            return view('home',compact('invoices'));
        }
        $clients = $user::whereHas('roles', function ($query) {
            $query->where('name','client');
        })->get();
        $invoices = Invoice::with('users')->get();

        return view('admin', compact('clients','invoices'));
    }
}
