@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Total</div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="post" action="{{route('update-invoice')}}">
                            @csrf
                            <input type="hidden" value="{{$invoice->id}}" name="id">
                            <input type="hidden" value="{{$invoice->user_id}}" name="user_id">
                            <div class="form-group">
                                <input type="number" class="form-control" id="exampleInputTotal"
                                       aria-describedby="totalHelp" placeholder="Enter total" name="total" value="{{$invoice->total}}">
                            </div>
                            <select class="form-control" name="user_id" disabled>
                                <option value="">Select Client</option>
                                @foreach($clients  as $client)
                                    <option {{($invoice->user_id == $client->id) ? 'selected' : ''}} value="{{$client->id}}">{{$client->name}}</option>
                                @endforeach
                            </select>
                            <br>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
