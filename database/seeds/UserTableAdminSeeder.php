<?php

use Illuminate\Database\Seeder;

class UserTableAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminId = \App\Role::where('name','admin')->select('id')->first();

        $user =  \App\User::create([
                'name' => 'Super Admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('secret'),
            ]);

        $user->roles()->attach($adminId);
    }
}
