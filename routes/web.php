<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth','checkRole:admin']], function () {
    Route::post('/store-invoice', 'AdminController@storeInvoice')->name('store-invoice');
    Route::get('/destroy-invoice/{id}', 'AdminController@destroyInvoice')->name('destroy-invoice');
    Route::get('/edit-invoice/{invoice}', 'AdminController@editInvoice')->name('edit-invoice');
    Route::get('/edit-client/{user}', 'AdminController@editClient')->name('edit-client');
    Route::post('/update-invoice', 'AdminController@updateInvoice')->name('update-invoice');
    Route::post('/create-client', 'AdminController@createClient')->name('create-client');
    Route::get('/client-view', 'AdminController@clientView')->name('client-view');
    Route::get('/destroy-client/{id}', 'AdminController@destroyClient')->name('destroy-client');
});

